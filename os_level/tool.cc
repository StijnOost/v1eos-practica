#include <iostream>
#include <string>

std::string flip(std::string line, std::string argument){ 
	std::string result = "";
	for (unsigned int i=0; i<line.length();i++)
	{
		if (line[i]>=65&&line[i]<=90)
		{
			result += char(int(line[i]+std::stoi(argument)-65)%26 +65);
		}
		else if(line[i]>=97&&line[i]<=122){
			result += char(int(line[i]+std::stoi(argument)-97)%26 +97);
		}
		else{
			result += line[i];
		}
	}
	return result; 
}

int main(int argc, char *argv[]){ 
	std::string line;

	if(argc != 2){ 
		std::cerr << "Deze functie heeft exact 1 argument nodig" << std::endl;
		return -1; 
	}

	while(std::getline(std::cin, line)){ 
		std::cout << flip(line, argv[1]) << std::endl; 
	} 

	return 0; 
}
