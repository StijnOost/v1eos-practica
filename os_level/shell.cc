#include "shell.hh"

int main()
{ std::string input;
  char prompt[1];
  while(true)
  {
	int fd = syscall(SYS_open, "command.txt", O_RDONLY, 0755);
	while(syscall(SYS_read, fd, prompt, 1)){                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
		std::cout << prompt; 
	}
    getline(std::cin, input);
											// Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;
	syscall(SYS_close,fd);
    if (std::cin.eof()) return 0; } }      // EOF is een exit
	
void new_file() // ToDo: Implementeer volgens specificatie.
{ 
	std::string input="";
	std::string bestandsnaam="";
	std::cout<<"Geef een string als bestandsnaam: ";
	getline(std::cin, bestandsnaam);
	int fd = syscall(SYS_open, bestandsnaam.c_str(), O_RDWR | O_CREAT, S_IRWXU);
	while(true){
		std::cout<<"Geef een regel text ";
		getline(std::cin, input);
		
		
		if(input.find("<EOF>") != std::string::npos){
			syscall(SYS_write,fd,input.c_str(),input.size()-5);
			break;
		}
		else{
			syscall(SYS_write,fd,input.c_str(),input.size());
		}
		input="\n";
		syscall(SYS_write,fd,input.c_str(),input.size());
		
	}
	syscall(SYS_close,fd);
	
}

void list() // ToDo: Implementeer volgens specificatie.
{
	int pid =syscall(SYS_fork);
	
	if (pid==0){
		const char *args[] = {"/bin/ls","-la",(char *) 0};
		int result = syscall(SYS_execve,"/bin/ls",args,0);
		std::cout<<"exit code "<<result;
	}
	else{
		
		syscall(SYS_wait4, pid,0,0,0);
	}
	
}

void find() // ToDo: Implementeer volgens specificatie.
{ 	
	int fd[2];
	syscall(SYS_pipe, &fd);
	std::string find="";
	std::cout<<"Wat wilt u zoeken?: ";
	getline(std::cin, find);
	int pid_1 =syscall(SYS_fork);

	
	
	
	if (pid_1==0){
		
		const char *args1[] = {"/usr/bin/find",".",NULL};
		syscall(SYS_dup2, fd[1], 1);
		syscall(SYS_close, fd[0]);
		syscall(SYS_execve,"/usr/bin/find",args1,NULL);
	}
	else if(pid_1>0){
		int pid_2 =syscall(SYS_fork);
		if (pid_2==0){
			const char *args2[] = {"/bin/grep",find.c_str(),NULL};
			
			syscall(SYS_dup2, fd[0], 0);
			syscall(SYS_close, fd[1]);
			syscall(SYS_execve,"/bin/grep",args2,NULL);
		}
		else{
			syscall(SYS_close, fd[1]);
			syscall(SYS_close, fd[0]);
			syscall(SYS_wait4, pid_1,NULL,NULL,NULL);
			syscall(SYS_wait4, pid_2,NULL,NULL,NULL);
		}
		
	}
	
	
}

void seek() // ToDo: Implementeer volgens specificatie.
{ 	
	
	int fd2 = syscall(SYS_open, "loop", O_RDWR | O_CREAT, S_IRWXU);
	syscall(SYS_write,fd2,"x",1);
	for(unsigned int i=0;i<5000000;i++){
		syscall(SYS_write,fd2,"\0",1);
	}
	syscall(SYS_write,fd2,"x",1);
	//std::cout << "Done" << std::endl;
	int fd1 = syscall(SYS_open, "seek", O_RDWR | O_CREAT, S_IRWXU);
	syscall(SYS_write,fd1,"x",1);
	syscall(SYS_lseek,fd1,5000000,1);					//seek is een stuk sneller
	syscall(SYS_write,fd1,"x",1);								//ls -lh is hetzelfde namelijk: 5.1MIB
	//std::cout << "Done" << std::endl;							//ls -lS seek is 1 byte korter
	
}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
